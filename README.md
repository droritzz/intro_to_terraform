## TERRAFORM BASICS LAB

### WHAT'S IN THE BOX?
This project is a basics lab to exercise simple terraform commands. The main.tf file will create the following setup:
- VPC on AWS with 254 IP
- one IP reserved to the Debian server
- one IP reserved to the Red Hat server

### REQUIREMENTS:
- terraform installed
- aws account
- aws credentials configured on a local machine

### HOW TO USE THIS PROJECT?

- check that all the requirements are met
- check the regions and instance IDs on AWS
- clone the project
- run `terraform init` and `terraform apply` 