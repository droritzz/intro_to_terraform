variable "aws_region" {
    default = "us-east-1"
    type = string
    description = "Deafault Zone for AWS"
}

variable "vpc_cidr_blk" {
    default = "172.31.0.0/16"
    type = string
    description = "Deafault CIDR block for VPC on AWS"
}