provider "aws" {
  profile = "default"
  region  = var.aws_region
}

resource "aws_vpc" "srv_vpc" {
  cidr_block = "${var.vpc_cidr_blk}"
  tags = {
    Name = "vpc terraform"
  }
}

resource "aws_subnet" "srv_subnet" {
  vpc_id            = aws_vpc.srv_vpc.id
  cidr_block        = "${var.vpc_cidr_blk}"
  availability_zone = "us-east-1a"

  tags = {
    Name = "subnet terraform"
  }
}

resource "aws_network_interface" "Debian_IF" {
  subnet_id   = aws_subnet.srv_subnet.id
  private_ips = ["172.31.1.100"]

  tags = {
    Name = "primary_network_interface_debian"
  }
}


resource "aws_instance" "Debian" {
  ami           = "ami-07d02ee1eeb0c996c"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.Debian_IF.id
    device_index         = 0
  }

  tags = {
    Name = "Debian Terraform VPC"
  }
}

resource "aws_network_interface" "RedHat_IF" {
  subnet_id   = aws_subnet.srv_subnet.id
  private_ips = ["172.31.1.101"]

  tags = {
    Name = "primary_network_interface_redhat"
  }
}


resource "aws_instance" "RedHat" {
  ami           = "ami-0b0af3577fe5e3532"
  instance_type = "t2.micro"

  network_interface {
    network_interface_id = aws_network_interface.RedHat_IF.id
    device_index         = 0
  }

  tags = {
    Name = "RedHat Terraform VPC"
  }
}


